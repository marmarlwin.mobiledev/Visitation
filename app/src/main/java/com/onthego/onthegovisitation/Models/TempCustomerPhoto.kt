package com.onthego.onthegovisitation

data class TempCustomerPhoto(
        val tempCustomerID : String,
        val tempCustomerPhoto : ByteArray
)